Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do

  resources :users do
    collection do
      post :signin
      post :signout
    end
  end
  get '/me', to: 'users#my_profile'

  resources :passwords do
    collection do
      post :request_forgot_password
    end
  end

  resources :projects do
    collection do
      get :available_user
    end
    member do
      get :show_member
      get :project_members
      post :add_member
      put :update_member
      delete :remove_member
    end
    resources :tasks do
    end
  end

  resources :roles do
  end
end

root 'users#index'
end
