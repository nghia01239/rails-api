class UserMailer < ApplicationMailer
  default from: 'zero@gmail.com'
  layout 'mailer'

  def test_mail
    mail(to: 'zero+1@gmail.com', subject: 'Test Mail')
  end

  def forgot_password(frontend_url, user)
    frontend_url = 'http://localhost:3000/password-recovery?'
    @token_check = User
    @reset_url = frontend_url + '?token=' + @token_check.SecureRandom.hex
     mail(to: user.email , subject: 'Forgot Password')
  end
end
