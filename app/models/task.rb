class Task < ApplicationRecord
  belongs_to :project
  belongs_to :task_status
  belongs_to :user

  validates :title, presence: true

  def as_json(opts = {})
    opts[:include] = [ { user: { only: %i[username] }}]
    super(opts)
  end
end