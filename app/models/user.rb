# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password
  validates :username, :email, presence: true
  validates :username, uniqueness: true, allow_blank: true
  validates :email, uniqueness: true, allow_blank: true

  has_many :project_users, dependent: :destroy
  has_many :projects, through: :project_users
  has_many :tasks, dependent: :destroy

  def as_json(opts = {})
    opts[:except] = [:password_digest] + (opts[:except] || [])
    super(opts)
  end

  def owner?(project)
    pu = project_user_in(project)
    pu&.role&.code == 'owner'
  end

  def maintainer?(project)
    pu = project_user_in(project)
    pu&.role&.code == 'maintainer'
  end

  def project_user_in(project)
    project_users.detect { |e| e.project_id == project.id }
  end

  def can_remove_user_from_project?(project_user)
    project = Project.powered_by(self).find_by_id(project_user.project_id)
    return false unless project

    user = project_user.user

    return false if user.owner?(project)

    return false if maintainer?(project) && user.maintainer?(project)

    true
  end

  def can_edit_user_from_project(project_user)
    project = Project.powered_by(self).find_by_id(project_user.project_id)
    return false unless project

    user = project_user.user

    return false if user.owner?(project)

    return false if maintainer?(project) && user.maintainer?(project)
  end
end
