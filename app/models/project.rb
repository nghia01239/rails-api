# frozen_string_literal: true
class Project < ApplicationRecord
  has_many :tasks, dependent: :destroy
  has_many :project_users, dependent: :destroy
  validate :name, :description

  scope :powered_by, lambda { |user|
    joins(:project_users)
      .where(
        project_users: {
          user: user,
          role_id: Role.where(code: %w[maintainer owner]).select(:id)
        }
      )
  }
end
