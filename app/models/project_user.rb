class ProjectUser < ApplicationRecord
  belongs_to :user
  belongs_to :project
  belongs_to :role

  validates :user_id, uniqueness: { scope: :project_id }
end
