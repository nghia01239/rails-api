# frozen_string_literal: true

class ApplicationController < ActionController::API
  before_action :authenticate_user!

  rescue_from 'ActiveRecord::RecordNotFound' do |_exception|
    render json: { message: 'Not found' }, status: 404
  end

  def not_found
    render json: { error: 'not_found' }
  end

  def current_user
    @current_user ||= User.find_by_id(user_id)
  end

  def authenticate_user!
    render json: { mesage: 'Unauthorized' }, status: 401 unless current_user
  end

  private

  def user_id
    return nil unless token

    decoded_token = JsonWebToken.decode(token)

    return nil unless decoded_token

    decoded_token[:user_id]
  end

  def token
    params[:auth_token] || request.headers['Authorization']
  end
end
