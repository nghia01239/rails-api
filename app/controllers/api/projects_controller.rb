# frozen_string_literal: true

module Api
  class ProjectsController < ApplicationController
    before_action :authenticate_user!
    # GET /projects
    # GET /projects.json
    def index
      @project = current_user.projects
      render json: @project, status: :ok
    end

    # GET /projects/1
    # GET /projects/1.json
    def show
      @project = current_user.projects.find(params[:id])
      render json: @project, status: :ok
    end

    def available_user
      user = User.where.not(id: project.project_users
        .select(:user_id)).find(params[:user_id])
      render json: user, status: :ok
    end

    def add_member
      project = Project.powered_by(current_user).find(params[:id])
      user = User.where.not(id: project.project_users
        .select(:user_id)).find(params[:user_id])
      role = Role.find_by_code(params[:role_code])
      ProjectUser.create(
        project_id: project.id,
        user_id: user.id,
        role: role
      )
      render json: project, status: :created
    end

    def remove_member
      project_user = ProjectUser.find_by_id(params[:id])
      if current_user.can_remove_user_from_project?(project_user)
        project_user&.destroy
        render json: { success: true }, status: :ok
      else
        render json: {
          errors: 'Current user can not remove this member'
        }, status: 422
      end
    end

    def update_member
      project_user = ProjectUser.find_by_id(params[:id])
      role = Role.find_by_code(params[:role_code])
      if current_user.can_remove_user_from_project?(project_user)
        project_user.attributes = {
          role: role
        }
        if project_user.save
          render json: { success: true }, status: :ok
        else
          render json: { success: false, errorsupdate: 'Can\'t update'
          }, status: 422
        end
      else
        render json: {
          errors: 'Current user can not remove this member'
        }, status: 422
      end
    end

    def project_members
      project = Project.find(params[:id])
      members = project
                .project_users
                .includes(:user, :role)
                .map do |project_user|
        {
          project_user_id: project_user.id,
          user_id: project_user.user_id,
          username: project_user.user.username,
          role: project_user.role.as_json(only: %i[name]),
          allow_remove: (project_user.role.code != 'owner') &&
            (current_user.owner?(project) ||
            (current_user.maintainer?(project) &&
            (project_user.role.code != 'maintainer')))
        }
      end
      roles = if current_user.owner?(project)
                Role.where.not(code: 'owner')
              elsif current_user.maintainer?(project)
                Role.where.not(code: %w[owner maintainer])
              else
                Role.none
              end
      users = User.where.not(id: project.project_users.select(:user_id))
      render json: {
        project: project,
        roles: roles,
        members: members,
        users: users
      }, status: :ok
    end

    # GET /projects/new
    def new
      @project = current_user.projects.new
      render json: @project, status: :ok
    end

    # GET /projects/1/edit
    def edit
      render json: @project, status: :ok
    end

    def create
      @project = Project.new(project_params)
      if @project.save
        owner_role = Role.where(code: 'owner').first_or_create(name: 'Owner')
        current_user.project_users.create(
          project: @project, role: owner_role)
        render json: @project, status: :created
      else
        render json: { errors: @project.errors.full_messages }, status:
        :unprocessable_entity
      end
    end

    # PATCH/PUT /projects/1
    # PATCH/PUT /projects/1.json
    def update
      @project = current_user.projects.find_by_id(params[:id])
      if @project.update(project_params)
        render json: @project, status: :ok
      else
        render json: { errors: @project.errors.full_messages }, status:
        :unprocessable_entity
      end
    end

    # DELETE /projects/1
    # DELETE /projects/1.json
    def destroy
      @project = current_user.projects.find_by_id(params[:id])
      if @project.destroy
        render json: @project, status: :ok
      else
        render json: { errors: @project.errors.full_messages }, status:
        :unprocessable_entity
      end
    end

    private

    # Never trust parameters from the scary internet
    def project_params
      params.require(:project).permit(:name, :description)
    end

    def project_user_params
      params.require(:project_user).permit(:project_id, :user_id, :role_id)
    end
  end
end
