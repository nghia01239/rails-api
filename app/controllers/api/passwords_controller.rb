# frozen_string_literal: true

module Api
  class PasswordsController < ApplicationController
    skip_before_action :authenticate_user!
    def request_forgot_password
      email = params[:user][:email]
      user = User.find_by(email: email)
      if user
      render json: user, status: :ok 
      else
      render json: {
        message: 'Can\'t find your email '
      }, status: 422
      end
    end

    def password_recovery
      @token = @token_check
    end

    def reset_password
      password = params[:user][:password]
      password_confirmation = params[:user][:password_confirmation]
      user = User.find_by(email: email)
      return render plain: 'User does not exist' unless user

      @message = 'Password is required' if password.blank?

      @message = 'Passwords do not match' if password != password_confirmation

      return render action: :password_recovery if @message.present?

      user.update(password: password)

      redirect_to new_signin_path
    end
  end
end
