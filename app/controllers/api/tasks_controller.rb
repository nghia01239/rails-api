# frozen_string_literal: true

module Api
  class TasksController < ApplicationController
    before_action :authenticate_user!
    before_action :set_project
    def index
      tasks = @project.tasks.includes(:user)
      render json: {
        tasks: tasks,
        project: @project
      }, status: :ok
    end

    def show
      task = @project.tasks.find_by_id(params[:id])

      render json: { task: task, project: @project }, status: :ok
    end

    def destroy
      task = @project.tasks.find(params[:id])
      if task.destroy
        render json: { task: task }, status: :ok
      else
        render json: { errors: @task.errors.full_messages}, status: :unprocessable_entity
      end
    end

    def create
      user = current_user
      @task = @project.tasks.new(full_tasks_params.merge(user: user))
      if @task.save
        render json: { task: @task }, status: :created
      else
        render json: { errors: @task.errors.full_messages }, status: :unprocessable_entity
      end
    end

    def update
      task = @project.tasks.find_by_id(params[:id])
      user = User.find_by_id(@project.tasks.select(:user_id))
      if task.update(full_tasks_params.merge( user: user))
        render json: { task: task }, status: :ok
      else
        render json: { errors: task.errors.full_messages }, status: :unprocessable_entity
      end
    end

    private

    def set_project
      @project = Project.find_by_id(params[:project_id])
    end

    def tasks_params
      params.require(:task).permit(:title, :description)
    end

    def full_tasks_params
      params.require(:task).permit(:title, :description, :task_status_id)
    end
  end
end