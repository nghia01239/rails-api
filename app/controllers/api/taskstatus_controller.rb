class Taskstatus < ApplicationController::Api
  def index
    # Generic API stuff here
    @taskstatuses = TaskStatus.all
    render json: @taskstatuses, status: :ok
  end
end