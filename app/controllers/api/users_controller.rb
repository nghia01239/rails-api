# frozen_string_literal: true

module Api
  class UsersController < ApplicationController
    skip_before_action :authenticate_user!, only: [:create, :signin]
    # GET /users
    def index
      @users = User.all
      render json: @users, status: :ok
    end
    def show
      @users = User.find(params[:id])

      render json: @users, status: :ok
    end
    # POST /users
    def create
      @user = User.new(user_params)
      if @user.save
        render json: @user, status: :created
      else
        render json: { errors: @user.errors.full_messages },
               status: :unprocessable_entity
      end
    end

    def my_profile
      return authenticate_user! unless user_id

      render json: {
        profile: current_user
      }, status: 200
    end

    def user_profile
      project = current_user.projects.find_by_id(params[:id])
      users = current_user.when.not(id: project.project_users.select(:user_id))
      render json: users, status: :ok
    end

    def signin
      user = User.find_by(username: sign_in_params[:username]) ||
             User.find_by(email: sign_in_params[:username])
      return render_user_not_found unless user

        if user.authenticate(sign_in_params[:password])
          return show_user(user)
        else
          render_user_not_found
        end
    end

    def render_user_not_found
      render json: {
        message: 'username or password is not correct. Did u forgot password??'
      }, status: 422
    end

    # PUT /users/{username}
    def update
      unless @user.update(user_params)
        render json: { errors: @user.errors.full_messages },
               status: :unprocessable_entity
      end
    end

    # DELETE /users/{username}
    def destroy
      @user.destroy
    end

    private

    def show_user(user)
      render json: {
        user: user, 
        token: JsonWebToken.encode({user_id: user.id})
      },
      status: :ok
    end

    def find_user
      @user = User.find_by_username!(params[:_username])
    rescue ActiveRecord::RecordNotFound
      render json: { errors: 'User not found' }, status: :not_found
    end

    def user_params
      params.require(:user).permit(
        :username, :email, :password, :password_confirmation
      )
    end

    def sign_in_params
      params.require(:user).permit(
        :username,
        :password
      )
    end
  end
end
