# frozen_string_literal: true

module Api
class RolesController < ApplicationController
  def index
    if current_user.owner?(project)
    elsif current_user.maintainer?(project)
    end
    @roles = Role.all
    render json: @roles, status: :ok
  end
end
end