class AddColumnToTasks < ActiveRecord::Migration[6.0]
  def change
    add_column :tasks, :task_status_id, :int
    add_column :tasks, :assignee, :string
    #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")

    remove_column :tasks, :completed_at
    remove_column :tasks, :doing_at

  end
end
