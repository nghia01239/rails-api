class CreateToDoList < ActiveRecord::Migration[6.0]
  def change
    create_table :to_do_lists do |t|
      t.string :title
      t.text :description
    end
  end
end
