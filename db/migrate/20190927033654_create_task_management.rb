class CreateTaskManagement < ActiveRecord::Migration[6.0]
  def change
    create_table :task_managements do |t|
      t.string :title
      t.string :description
      t.datetime :completed_at
      t.datetime :doing_at
      t.integer :todolist_id

    end
  end
end
