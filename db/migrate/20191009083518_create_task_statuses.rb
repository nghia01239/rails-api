class CreateTaskStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :task_statuses do |t|
      t.string :name
      t.string :code
      t.integer :task_id

      t.timestamps
    end
  end
end
