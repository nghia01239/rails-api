class CreateProjectUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :project_users do |t|
      t.integer :project_id
      t.integer :user_id
      t.integer :role_id

      t.timestamps
    end
    add_index :project_users, :project_id
    add_index :project_users, :user_id
    add_index :project_users, :role_id
  end
end
