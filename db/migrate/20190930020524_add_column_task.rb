class AddColumnTask < ActiveRecord::Migration[6.0]
  def change
    # Add
    add_column :task_managements, :project_id, :integer

    # Remove
    remove_column :task_managements, :todolist_id, :integer

  end
end
